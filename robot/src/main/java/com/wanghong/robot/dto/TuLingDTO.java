package com.wanghong.robot.dto;

import lombok.Data;

@Data
public class TuLingDTO {
	private String code;
	private String text;
}
