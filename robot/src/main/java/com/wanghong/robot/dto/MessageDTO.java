package com.wanghong.robot.dto;

import lombok.Data;

@Data
public class MessageDTO {
	// 用户
	private String username;
	// 头像
	private String avatar;
	// id
	private String id;
	// 类型
	private String type;
	// 内容
	private String content;
	// 时间戳
	private String timestamp;
}
