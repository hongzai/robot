package com.wanghong.robot.im;

import java.util.HashMap;
import java.util.Map;

import cn.hutool.http.HttpRequest;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSON;
import com.wanghong.robot.dto.TuLingDTO;

@Component
public class TuLingRobot {
    private final static String APIKEY = "7f00a489f3019ad3a5b642c95b0cf733";
    private final static String TULING_URL = "http://www.tuling123.com/openapi/api";

    public static String sendMessage(String message) throws Exception {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("key", APIKEY);
        dataMap.put("info", message);
        String response = HttpRequest.post(TULING_URL).body(JSON.toJSONString(dataMap)).execute().body();
        System.out.println("robot--->" + response);
        TuLingDTO tuLing = JSON.parseObject(response, TuLingDTO.class);
        return tuLing.getText();
    }

    public static void main(String[] args) throws Exception {
        TuLingRobot.sendMessage("今晚吃什么");
    }
}
